import javax.swing.JFrame;

public class TitlesFrame extends JFrame
{
  /**
   * Конструктор класса
   */
  public TitlesFrame() // реализация метода initUI перенесена в конструктор
  {
    setTitle("Кривые фигуры");
    setDefaultCloseOperation(3);
    add(new TitlesPanel(91));
    setSize(350, 350);
    setLocationRelativeTo(null);
  }
  
  /**
   * Метод инициализирует пользовательский интерфейс
     * @param args
   */
//  private void initUI()
//  {
//
//  }
  
  public static void main(String[] args)
  {
    javax.swing.SwingUtilities.invokeLater(() -> { // использован лямбда синтаксис
        TitlesFrame ps = new TitlesFrame();
        ps.setVisible(true);
    });
  }
}